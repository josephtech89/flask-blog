from flask import render_template, request, Blueprint
from flaskblog.models import Post

main = Blueprint('main', __name__)


@main.route('/')
@main.route('/home')
def homepage():
    page_num = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(per_page=2, page=page_num)
    return render_template('home.html', posts=posts)


@main.route('/about')
def aboutpage():
    return render_template('about.html', title="About")