import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flaskblog import mail
from flask_mail import Message


def save_picture(profile_pic):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(profile_pic.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(current_app.root_path + '/static/profile_pics/' + picture_fn)

    output_size = (125, 125)
    i = Image.open(profile_pic)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Reset Request Password', sender='noreply@example.com', recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
    {url_for('reset_token', token=token, _external=True)}
    If you did not make this request simply ignore this email and no changes will be made.
    '''
    mail.send(msg)
